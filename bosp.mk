
ifdef CONFIG_BENCHMARK_WCET

# Targets provided by this project
.PHONY: benchmark_wcet clean_benchmark_wcet

# Add this to the "contrib_testing" target
benchmarks: benchmark_wcet
clean_benchmarks: clean_benchmark_wcet

BBQUE_WCET_HOME := benchmarks/wcet

benchmark_wcet: external
	@echo
	@echo "==== Building Malardalen WCET Benchmark Suite ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(BBQUE_WCET_HOME)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(BBQUE_WCET_HOME)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(BBQUE_WCET_HOME)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(BBQUE_WCET_HOME)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_benchmark_wcet:
	@echo
	@echo "==== Clean-up Malardalen WCET Benchmark Suite ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-benchmark-wcet ] || \
		rm -f $(BUILD_DIR)/usr/bin/bbque-benchmark-wcet*
	@rm -rf $(BBQUE_WCET_HOME)/build
	@echo

else # CONFIG_BENCHMARK_WCET

benchmark_wcet:
	$(warning contib WCET benchmarks disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_BENCHMARK_WCET

