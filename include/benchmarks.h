#ifndef BENCHMARKS_H
#define BENCHMARKS_H

#ifdef __cplusplus
extern "C" {
#endif

#define BENCH(x) extern int wcet_ ## x(void)

	BENCH(adpcm);
	BENCH(bs);
	BENCH(bsort100);
	BENCH(cnt);
	BENCH(cover);
	BENCH(crc);
	BENCH(duff);
	BENCH(edn);
	BENCH(expint);
	BENCH(fac);
	BENCH(fdct);
	BENCH(fft1);
	BENCH(fibcall);
	BENCH(fir);
	BENCH(insertsort);
	BENCH(janne_complex);
	BENCH(jfdctint);
	BENCH(lcdnum);
	BENCH(lms);
	BENCH(ludcmp);
	BENCH(matmult);
	BENCH(minver);
	BENCH(ndes);
	BENCH(ns);
	BENCH(nsichneu);
	BENCH(qsort_exam);
	BENCH(qurt);
	BENCH(recursion);
	BENCH(select);
	BENCH(sqrt);
	BENCH(st);
	BENCH(statemate);
	BENCH(ud);

#ifdef __cplusplus
}
#endif


#endif // BENCHMARKS_H

