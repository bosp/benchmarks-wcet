/**
 *       @file  WCET_exc.h
 *      @brief  The WCET BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef WCET_EXC_H_
#define WCET_EXC_H_

#include <bbque/bbque_rtexc.h>
#include <vector>

using bbque::rtlib::BbqueRTEXC;

class WCET : public BbqueRTEXC {

public:

	WCET(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib,
			RTLIB_RT_Level_t rt_level,
			std::string benchmark,
			unsigned int single_run_nr,
			unsigned int outer_run_nr,
			unsigned int ignore_after_conf,
			uint64_t deadline
			) noexcept;

private:

	bool just_reconfigured;

	int (*benchmark_fun)(void);

	const std::string benchmark;
	const unsigned int single_run_nr;
	const unsigned int outer_run_nr;
	const unsigned int ignore_after_conf;
	const uint64_t deadline;

	std::vector<uint64_t> results;

	uint64_t SaveSingleValue(const timespec*);

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();

};

#endif // WCET_EXC_H_
