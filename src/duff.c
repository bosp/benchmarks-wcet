/* $Id: duff.c,v 1.2 2005/04/04 11:34:58 csg Exp $ */



/*----------------------------------------------------------------------

 *  WCET Benchmark created by Jakob Engblom, Uppsala university,

 *  February 2000.

 *

 *  The purpose of this benchmark is to force the compiler to emit an

 *  unstructured loop, which is usually problematic for WCET tools to

 *  handle.

 *

 *  The execution time should be constant.

 *

 *  The original code is "Duff's Device", see the Jargon File, e.g. at

 *  http://www.tf.hut.fi/cgi-bin/jargon.  Created in the early 1980s

 *  as a way to express loop unrolling in C.

 *

 *----------------------------------------------------------------------*/



#define ARRAYSIZE  1000

#define INVOCATION_COUNT 43	/* exec time depends on this one! */



static void duffcopy( char *to, char *from, int count)
{

  int n=(count+7)/8;
  switch(count%8){
  case 0: do{     *to++ = *from++;
	  case 7:         *to++ = *from++;
	  case 6:         *to++ = *from++;
	  case 5:         *to++ = *from++;
	  case 4:         *to++ = *from++;
	  case 3:         *to++ = *from++;
	  case 2:         *to++ = *from++;
	  case 1:         *to++ = *from++;
	  } while(--n>0);
  }
}


static void initialize( char *arr, int length)
{
  int i;
  for(i=0;i<length;i++)
    {
      arr[i] = length-i;
    }
}

static char source[ARRAYSIZE];
static char target[ARRAYSIZE];


int wcet_duff(void)
{

  initialize( source, ARRAYSIZE );

  duffcopy( source, target, INVOCATION_COUNT );

	return 0;
}


