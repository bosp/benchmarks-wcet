/**
 *       @file  WCET_main.cc
 *      @brief  The WCET BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <cstdio>
#include <iostream>
#include <random>
#include <cstring>
#include <memory>

#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "version.h"
#include "WCET_exc.h"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "WCET"

namespace bu = bbque::utils;
namespace po = boost::program_options;

/**
 * @brief A pointer to an EXC
 */
std::unique_ptr<bu::Logger> logger;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<BbqueRTEXC> pBbqueRTEXC_t;

/**
 * The decription of each WCET parameters
 */
po::options_description opts_desc("WCET Configuration Options");

/**
 * The map of all WCET parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief The application configuration file
 */
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/WCET.conf" ;

/**
 * @brief The recipe to use for all the EXCs
 */
std::string recipe;

/**
 * @brief The RT level of application
 */
std::string conf_rt_level;

/**
 * @brief The benchmark to execute
 */
std::string conf_benchmark;

/**
 * @brief The number of single run in a onRun
 */
unsigned int conf_single_run_n;

/**
 * @brief The number of onRun to execute
 */
unsigned int conf_outer_run_n;

/**
 * @brief Number of sample to ignore after reconfiguration 
 */
unsigned int conf_ignore_after_conf;

/**
 * @brief Deadline in ns
 */
uint64_t conf_deadline;

/**
 * @brief The EXecution Context (EXC) registered
 */
pBbqueRTEXC_t pexc;

void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
		po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
		po::notify(opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "WCET (ver. " << g_git_version << ")\n";
		std::cout << "Copyright (C) 2011 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]) {

	opts_desc.add_options()
		("help,h", "print this help message")
		("version,v", "print program version")

		("conf,C", po::value<std::string>(&conf_file)->
			default_value(conf_file),
			"WCET configuration file")

		("recipe,r", po::value<std::string>(&recipe)->
			default_value("WCET"),
			"recipe name (for all EXCs)")

		("rtlevel,R", po::value<std::string>(&conf_rt_level)->
		default_value("SOFT"),
		"RT Level of application: SOFT or HARD")

		("benchmark,b", po::value<std::string>(&conf_benchmark)->
		required(),
		"The benchmark to execute")

		("single-runs,s", po::value<unsigned int>(&conf_single_run_n)->
		default_value(100),
		"Number of test case in a single onRun")

		("outer-runs,S", po::value<unsigned int>(&conf_outer_run_n)->
		default_value(100),
		"Number of onRun to execute")

		("ignore-after-conf,i", po::value<unsigned int>(&conf_ignore_after_conf)->
		default_value(5),
		"Number of sample to ignore after reconfiguration")

		("deadline,d", po::value<uint64_t>(&conf_deadline)->
		default_value(1000000),
		"Deadline in ns")

	;

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("wcet");

	ParseCommandLine(argc, argv);

	RTLIB_RT_Level_t rt_level;
	if (conf_rt_level == "SOFT") {
		rt_level = RT_SOFT;
	}
#ifdef CONFIG_BBQUE_RT_HARD
	else if (conf_rt_level == "HARD") {
		rt_level = RT_HARD;
	}
#endif
	else {
		std::cout << "Invalid real-time level" << std::endl;
		::exit(EXIT_FAILURE);
	}

	// Welcome screen
	logger->Info(".:: WCET (ver. %s) ::.", g_git_version);
	logger->Info("Built: " __DATE__  " " __TIME__);

	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	logger->Info("STEP 0. Initializing RTLib, application [%s]...",
			::basename(argv[0]));

	if ( RTLIB_Init(::basename(argv[0]), &rtlib) != RTLIB_OK) {
		logger->Fatal("Unable to init RTLib (Did you start the BarbequeRTRM daemon?)");
		return RTLIB_ERROR;
	}

	assert(rtlib);

	logger->Info("STEP 1. Registering EXC using [%s] recipe...",
			recipe.c_str());
	pexc = pBbqueRTEXC_t(new WCET("WCET", recipe, rtlib, rt_level, conf_benchmark,
					conf_single_run_n, conf_outer_run_n, conf_ignore_after_conf, conf_deadline));
	if (!pexc->isRegistered()) {
		logger->Fatal("Registering failure.");
		return RTLIB_ERROR;
	}


	logger->Info("STEP 2. Starting EXC control thread...");
	pexc->Start();


	logger->Info("STEP 3. Waiting for EXC completion...");
	pexc->WaitCompletion();


	logger->Info("STEP 4. Disabling EXC...");
	pexc = NULL;

	logger->Info("===== WCET DONE! =====");
	return EXIT_SUCCESS;

}

