/* $Id: recursion.c,v 1.2 2005/04/04 11:34:58 csg Exp $ */
/* Generate an example of recursive code, to see  *
 * how it can be modeled in the scope graph.      */
/* self-recursion  */

static int fib(int i)
{
  if(i==0)
    return 1;
  if(i==1)
    return 1;
  return fib(i-1) + fib(i-2);
}

/* mutual recursion */
static int anka(int);
static int kalle(int i)
{
  if(i<=0)
    return 0;
  else
    return anka(i-1);
}

static int anka(int i)
{
  if(i<=0)
    return 1;
  else
    return kalle(i-1);
}


int wcet_recursion(void)
{
	volatile int in;
	in = fib(25);
	return in;
}
