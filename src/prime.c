/* MDH WCET BENCHMARK SUITE. */

/* Changes:
 * JG 2005/12/08: Prototypes added, and changed exit to return in main.
 */

typedef  unsigned char  bool;
typedef  unsigned int   uint;

static bool divides (uint n, uint m);
static bool even (uint n);
static bool prime (uint n);
static void swap (uint* a, uint* b);

static bool divides (uint n, uint m) {
  return (m % n == 0);
}

static bool even (uint n) {
  return (divides (2, n));
}

static bool prime (uint n) {
  uint i;
  if (even (n))
      return (n == 2);
  for (i = 3; i * i <= n; i += 2) { 
      if (divides (i, n)) /* ai: loop here min 0 max 357 end; */
          return 0; 
  }
  return (n > 1);
}

static void swap (uint* a, uint* b) {
  uint tmp = *a;
  *a = *b; 
  *b = tmp;
}

int wcet_prime (void) {
  uint x =  21649;
  uint y = 513239;
  swap (&x, &y);
  return (!(prime(x) && prime(y)));
}

