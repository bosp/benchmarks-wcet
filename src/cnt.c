/* $Id: cnt.c,v 1.3 2005/04/04 11:34:58 csg Exp $ */



#define MAXSIZE 100


// Typedefs
typedef int matrix [MAXSIZE][MAXSIZE];


// Forwards declarations
int main(void);
static int Test(matrix);
static int Initialize(matrix);
static void Sum(matrix);
static int RandomInteger(void);

static int Seed = 0;

static matrix Array;

static int Postotal, Negtotal, Poscnt, Negcnt;


// The main function
int wcet_cnt (void)
{
   Test(Array);
   return 1;

}

static int Test(matrix Array)
{
   Initialize(Array);
   Sum(Array);

   return 0;

}


// Intializes the given array with random integers.
int Initialize(matrix Array)
{
   register int OuterIndex, InnerIndex;
   for (OuterIndex = 0; OuterIndex < MAXSIZE; OuterIndex++) //100 + 1
      for (InnerIndex = 0; InnerIndex < MAXSIZE; InnerIndex++) //100 + 1
         Array[OuterIndex][InnerIndex] = RandomInteger();

   return 0;
}


static void Sum(matrix Array)
{
  register int Outer, Inner;

  int Ptotal = 0; /* changed these to locals in order to drive worst case */
  int Ntotal = 0;
  int Pcnt = 0;
  int Ncnt = 0;

  for (Outer = 0; Outer < MAXSIZE; Outer++) //Maxsize = 100
    for (Inner = 0; Inner < MAXSIZE; Inner++)

      if (Array[Outer][Inner] >= 0) {
	  Ptotal += Array[Outer][Inner];
	  Pcnt++;
	}

	else {
	  Ntotal += Array[Outer][Inner];
	  Ncnt++;
	}

  Postotal = Ptotal;
  Poscnt = Pcnt;
  Negtotal = Ntotal;
  Negcnt = Ncnt;
}


// Generates random integers between 0 and 8095
int RandomInteger(void)
{
   Seed = ((Seed * 133) + 81) % 8095;

   return Seed;
}
