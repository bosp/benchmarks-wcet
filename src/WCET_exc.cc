/**
 *       @file  WCET_exc.cc
 *      @brief  The WCET BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include "WCET_exc.h"
#include "benchmarks.h"

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <numeric>

#include <bbque/utils/utility.h>
#include <bbque/utils/assert.h>

WCET::WCET(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		RTLIB_RT_Level_t rt_level,
		std::string benchmark,
		unsigned int single_run_nr,
		unsigned int outer_run_nr,
		unsigned int ignore_after_conf,
		uint64_t deadline
		) noexcept :
		BbqueRTEXC(name, recipe, rtlib, rt_level),
		benchmark(benchmark),
		single_run_nr(single_run_nr),
		outer_run_nr(outer_run_nr),
		ignore_after_conf(ignore_after_conf),
		deadline(deadline)
		{

	logger->Warn("New WCET::WCET()");


	logger->Info("EXC Unique IDentifier (UID): %u", GetUniqueID());

}

RTLIB_ExitCode_t WCET::onSetup() {
	logger->Warn("WCET::onSetup()");

	logger->Info("Selected benchmark: %s", benchmark.c_str()); 
	logger->Info("Parameters: nr. per onRun: %d, nr. onRun: %d, nr. to ignore: %d", 
		single_run_nr, outer_run_nr, ignore_after_conf);

	const char* sel_bench = benchmark.c_str();

#define CASE_BENCH(bench) case ConstHashString(#bench): benchmark_fun = wcet_ ## bench; break

	switch(ConstHashString(sel_bench)) {

		CASE_BENCH(adpcm);
		CASE_BENCH(bs);
		CASE_BENCH(bsort100);
		CASE_BENCH(cnt);
		CASE_BENCH(cover);
		CASE_BENCH(crc);
		CASE_BENCH(duff);
		CASE_BENCH(edn);
		CASE_BENCH(expint);
		CASE_BENCH(fac);
		CASE_BENCH(fdct);
		CASE_BENCH(fft1);
		CASE_BENCH(fibcall);
		CASE_BENCH(fir);
		CASE_BENCH(insertsort);
		CASE_BENCH(janne_complex);
		CASE_BENCH(jfdctint);
		CASE_BENCH(lcdnum);
		CASE_BENCH(lms);
		CASE_BENCH(ludcmp);
		CASE_BENCH(matmult);
		CASE_BENCH(minver);
		CASE_BENCH(ndes);
		CASE_BENCH(ns);
		CASE_BENCH(nsichneu);
		CASE_BENCH(qsort_exam);
		CASE_BENCH(qurt);
		CASE_BENCH(recursion);
		CASE_BENCH(select);
		CASE_BENCH(sqrt);
		CASE_BENCH(st);
		CASE_BENCH(statemate);
		CASE_BENCH(ud);

		default:
			logger->Error("Benchmark %s not found.", sel_bench);
			return RTLIB_ERROR;
	}

#undef CASE_BENCH

	results.reserve(single_run_nr);

	return RTLIB_OK;
}

static inline void timespec_diff(struct timespec *start, struct timespec *stop,
					struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}


RTLIB_ExitCode_t WCET::onConfigure(int8_t awm_id) {

	logger->Warn("WCET::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);

	just_reconfigured = true;

	return RTLIB_OK;
}

uint64_t WCET::SaveSingleValue(const timespec* value) {

	bbque_assert(value->tv_sec > 0);
	bbque_assert(value->tv_nsec > 0);

	uint64_t new_value = value->tv_nsec;
	new_value += ((uint64_t) value->tv_sec) * 1000000000ULL;

	results.push_back(new_value);

	return new_value;
}

RTLIB_ExitCode_t WCET::onRun() {

	if (Cycles() >= outer_run_nr)
		return RTLIB_EXC_WORKLOAD_NONE;

	timespec timing_begin, timing_end, timing_diff;

	for (unsigned int i=0; i < single_run_nr; i++) {
		clock_gettime(CLOCK_REALTIME, &timing_begin);
		benchmark_fun();
		clock_gettime(CLOCK_REALTIME, &timing_end);
		timespec_diff(&timing_begin, &timing_end, &timing_diff);

		uint64_t value = SaveSingleValue(&timing_diff);

		if ( just_reconfigured ) {
			// Ignore the first data after the reconfiguration
			if (i < ignore_after_conf) continue;
			just_reconfigured = false;
		}

		if (value > deadline) {
			logger->Warn("Deadline miss");
			return RTLIB_OK;	 // Not good
		}
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t WCET::onMonitor() {

	bbque_assert(results.size() > 0);

	uint64_t wcet = *std::max_element(results.cbegin(), results.cend());

	int64_t margin = (int64_t)deadline - (int64_t)wcet;

	if (margin < 0) {
		SetGoalGap(-100);	// Very bad, loading deadline
	} else {
		SetGoalGap( margin * 100 / (int64_t)deadline );
	}

	uint64_t bcet = *std::min_element(results.cbegin(), results.cend());
	uint64_t acet = std::accumulate(results.cbegin(), results.cend(), 0) / results.size();

	uint64_t sq_sum = std::inner_product(results.cbegin(), results.cend(), results.cbegin(), 0.0);
	uint64_t stdev = std::sqrt(sq_sum / results.size() - acet * acet);


	logger->Notice("%4d: NR [%3d] WCET [%7lld] ACET [%7lld] BCET [%7lld] stddev [%7lld]", Cycles(),
			results.size(), wcet, acet, bcet, stdev);

	results.clear();

	return RTLIB_OK;
}

RTLIB_ExitCode_t WCET::onRelease() {

	logger->Warn("WCET::onRelease()  : exit");

	return RTLIB_OK;
}
